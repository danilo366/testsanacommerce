﻿using Interface;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestSanaDanilo.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductRepository _productRepository;

        public ProductController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }
        // GET: Product
        public ActionResult Index()
        {
            List<ProductModel> listOfproduct = null;
            if (HttpContext.Session["storage"] != null && HttpContext.Session["storage"].Equals("inmemory"))
            {
                ViewBag.isInMemorySelected = true;

                if (HttpContext.Session["productListInMemory"] != null)
                    listOfproduct = HttpContext.Session["productListInMemory"] as List<ProductModel>;
                else
                    listOfproduct = new List<ProductModel>();
            }
            else
                listOfproduct = _productRepository.GetProducts().ToList();

            return View(listOfproduct);
        }
    }
}