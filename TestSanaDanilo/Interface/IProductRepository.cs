﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    public interface IProductRepository
    {
        ProductModel CreateProduct(ProductModel productModel);
        IEnumerable<ProductModel> GetProducts();

        ProductModel GetProductByProductNumber(int productNumber);
    }
}
