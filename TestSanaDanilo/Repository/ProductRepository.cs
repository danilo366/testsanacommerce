﻿using Data;
using Interface;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Repository
{
    public class ProductRepository : IProductRepository
    {
        private readonly TestDBEntities _testDBEntities;

        public ProductRepository(TestDBEntities testDBEntities)
        {
            _testDBEntities = testDBEntities;
        }
        public ProductModel CreateProduct(ProductModel productModel)
        {
            
            try                
            {             
                Product createProduct = new Product
                {
                    ProductNumber = productModel.ProductNumber,
                    Title = productModel.Title,
                    Price = productModel.Price
                };

                _testDBEntities.Product.Add(createProduct);
                _testDBEntities.SaveChanges();

            }
            catch (Exception ex)
            {

                throw ex;
            }

            return productModel;
        }

        public ProductModel GetProductByProductNumber(int productNumber)
        {
            ProductModel productModel = null;
            try
            {
                Product product = _testDBEntities.Product.FirstOrDefault(p => p.ProductNumber.Equals(productNumber));
                if (product != null)
                    productModel = new ProductModel { ProductNumber = product.ProductNumber, Title = product.Title, Price = product.Price };

            }
            catch (Exception e)
            {

                throw e;
            }
            return productModel;
        }

        public IEnumerable<ProductModel> GetProducts()
        {
            List<ProductModel> listProducts = null;
            try
            {
                listProducts = _testDBEntities.Product.Select(p => new ProductModel {
                ProductNumber = p.ProductNumber,
                Title = p.Title,
                Price = p.Price,
                }).ToList();
            }
            catch (Exception e)
            {

                throw e;
            }
            return listProducts;
        }
    }
}
