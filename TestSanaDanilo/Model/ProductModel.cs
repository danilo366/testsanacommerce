﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class ProductModel
    {
        [Display(Name = "Product Number")]
        [Required(ErrorMessage = "Product Number is required.")]
        public int ProductNumber { get; set; }

        [Display(Name = "Title")]
        [Required(ErrorMessage = "Title is required.")]
        [StringLength(50, ErrorMessage = "Max length exceeded")]
        public string Title { get; set; }

        [Display(Name = "Price")]
        [Required(ErrorMessage = "Price is required.")]
        [Range(0, 99999999.99)]
        public decimal Price { get; set; }
    }
}
